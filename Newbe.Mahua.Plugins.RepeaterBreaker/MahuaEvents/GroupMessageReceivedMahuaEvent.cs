﻿using Newbe.Mahua.MahuaEvents;
using Newbe.Mahua.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Newbe.Mahua.Plugins.RepeaterBreaker.MahuaEvents {
    public static class Common {
        //存储全局静态变量
        public static int repeatTime = 0;   //复读次数，初值为0
        public static string msg = "000";   //截获群消息用于复读行为检测
        public static int repeatExecuate = 2;   //检测复读次数，可通过私戳消息自定义
        public static IList<string> repeaterList = new List<string>();   //参与复读的复读机名单
        public static int execuateTime = 1;   //禁言时间
        public static int execuateMode = 2;   //执行模式
        public static Queue<Tuple<string, string>> banHistory = new Queue<Tuple<string, string>>();
        public static Queue<Tuple<string, string>> adminHistory = new Queue<Tuple<string, string>>();
    }
    /// <summary>
    /// 群消息接收事件
    /// </summary>
    public class GroupMessageReceivedMahuaEvent
        : IGroupMessageReceivedMahuaEvent {
        private readonly IMahuaApi _mahuaApi;

        private const int HISTORY_SIZE = 100;
        private const int TOP_N = 10;

        private string imageName(string msg) {
            Regex r = new Regex(@"^\[CQ:image(,.*=[^\],]*)*,file=([^\],]*)(,.*=[^\],]*)*]$");
            if (r.IsMatch(msg)) {
                return r.Replace(msg, "$2");
            }
            return null;
        }

        private bool isRepeating(string msg) {
            if (msg.Length != Common.msg.Length) {
                return false;
            }

            var oldImage = imageName(Common.msg);
            var newImage = imageName(msg);
            if (!String.IsNullOrEmpty(oldImage) && !String.IsNullOrEmpty(newImage)) {
                return Path.GetFileNameWithoutExtension(oldImage) == Path.GetFileNameWithoutExtension(newImage);
            }

            bool checkFuzzy = msg.Length >= 3;
            for (var i = 0; i < msg.Length; i++) {
                if (msg[i] != Common.msg[i]) {
                    if (!checkFuzzy) {
                        return false;
                    }
                    else {
                        checkFuzzy = false;
                    }
                }
            }

            return true;
        }

        private void banUser(GroupMessageReceivedContext context, string user) {
#if false
            var info = _mahuaApi.GetGroupMemberInfo(context.FromGroup, user);
            var nick = nickFromInfo(info, user);
            if (info.Authority >= GroupMemberAuthority.Manager) {
                Common.adminHistory.Enqueue(new Tuple<string, string>(user, nick));
                while (Common.adminHistory.Count > HISTORY_SIZE) {
                    Common.adminHistory.Dequeue();
                }
                return;
            }
#else
            var nick = user;
#endif
            Common.banHistory.Enqueue(new Tuple<string, string>(user, nick));
            while (Common.banHistory.Count > HISTORY_SIZE) {
                Common.banHistory.Dequeue();
            }

            var time = TimeSpan.FromMinutes(Common.execuateTime);
            _mahuaApi.BanGroupMember(context.FromGroup, user, time);
        }

        private string nickFromInfo(GroupMemberInfo info, string qq) {
            if (info == null) {
                return qq;
            }
            if (string.IsNullOrEmpty(info.InGroupName)) {
                return info.NickName;
            }
            return info.InGroupName;
        }

        private List<Tuple<string, int, string>> rank(Queue<Tuple<string, string>> history, string group) {
            var count = new Dictionary<string, Tuple<string, int>>();
            foreach (var user in history) {
                if (count.ContainsKey(user.Item1)) {
                    count[user.Item1] = new Tuple<string, int>(user.Item2, count[user.Item1].Item2 + 1);
                }
                else {
                    count[user.Item1] = new Tuple<string, int>(user.Item2, 1);
                }
            }

            var list = new List<Tuple<string, int, string>>();
            foreach (var entry in count) {
                list.Add(new Tuple<string, int, string>(entry.Key, entry.Value.Item2, entry.Value.Item1));
            }
            list.Sort((lhs, rhs) => rhs.Item2.CompareTo(lhs.Item2));
            if (list.Count > TOP_N) {
                list.RemoveRange(TOP_N, list.Count - TOP_N);
            }

            return list;
        }

        private IGroupMessageStep2 rankMessage(IGroupMessageStep2 msg, List<Tuple<string, int, string>> list) {
            for (var i = 0; i < list.Count; i++) {
                msg = msg.Newline().Text(String.Format("{0}：{1} {2}次", i + 1, list[i].Item3, list[i].Item2));
            }
            return msg;
        }

        private void sendHistory(GroupMessageReceivedContext context) {
            var list = rank(Common.banHistory, context.FromGroup);
            var adminList = rank(Common.adminHistory, context.FromGroup);
            var msg = _mahuaApi.SendGroupMessage(context.FromGroup).Text("");
            if (list.Count + adminList.Count == 0) {
                msg.Text("最近无人复读").Done();
            }
            else {
                if (list.Count > 0) {
                    msg = msg.Text("复读机禁言排名：");
                    msg = rankMessage(msg, list);
                    msg = msg.Newline();
                }
                if (adminList.Count > 0) {
                    msg = msg.Text("复读的苟管理：");
                    msg = rankMessage(msg, adminList);
                    msg = msg.Newline();
                }
                msg.Done();
            }
        }

        public GroupMessageReceivedMahuaEvent(
            IMahuaApi mahuaApi) {
            _mahuaApi = mahuaApi;
        }

        public void ProcessGroupMessage(GroupMessageReceivedContext context) {
            // Filter any system message or any message from ourselves.
            if (context.FromQq == "1000000" || context.FromQq == _mahuaApi.GetLoginQq()) {
                return;
            }

            if (context.Message == "复读统计") {
                sendHistory(context);
                return;
            }

            // Check if people is repeating.
            if (isRepeating(context.Message)) {
                Common.repeatTime++;
                Common.repeaterList.Add(context.FromQq);
            }
            else {
                Common.repeaterList.Clear();
                Common.repeatTime = 0;
            }
            Common.msg = context.Message;
            // Debug
            // _mahuaApi.SendGroupMessage(context.FromGroup, "复读计数：" + Common.repeatTime.ToString());

            //复读事件触发，根据执行模式进行禁言操作
            if (Common.repeatTime >= Common.repeatExecuate) {
                switch (Common.execuateMode) {
                    case 0:
                        banUser(context, context.FromQq);

                        Common.repeatTime = 0;
                        Common.repeaterList.Clear();
                        break;

                    case 1:
                        Random ran = new Random();
                        int RandKey = ran.Next(0, Common.repeaterList.Count - 1);
                        banUser(context, Common.repeaterList[RandKey]);

                        Common.repeatTime = 0;
                        Common.repeaterList.Clear();
                        break;

                    case 2:
                        HashSet<string> banned = new HashSet<string>();
                        foreach (var repeater in Common.repeaterList) {
                            if (!banned.Contains(repeater)) {
                                banned.Add(repeater);
                                banUser(context, repeater);
                            }
                        }

                        // Don't reset the counter, so we can ban everyone afterwards.
                        Common.repeaterList.Clear();
                        break;
                }
            }
        }
    }
}
